var create = document.getElementById("create");
var read = document.getElementById("read");
var body = document.querySelector("body");

body.addEventListener('click', function(e) {
    if(e.target.getAttribute('id') ===  'del') {
        const id = e.target.getAttribute('data-id');//
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    readFunction();
                }
            }
        };

        var defaultParams = {
            id: id
        };
        var params = new URLSearchParams(defaultParams);

        xhr.open('delete', 'http://localhost:8080/', true);
        xhr.send(params);
    }

    if(e.target.getAttribute('id') ===  'upd') {  //Update
        const id = e.target.getAttribute('data-id');
        const updateName = document.querySelector('[data-name-id="'+id+'"]');
        const updateLastName = document.querySelector('[data-last_name-id="'+id+'"]');
        const updateAge = document.querySelector('[data-age-id="'+id+'"]');
        const updateCity = document.querySelector('[data-city-id="'+id+'"]');

        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    console.log( xhr.responseText );
                }
            }
        };
        var defaultParams = {
            id: id,
            name: updateName.value,
            last_name: updateLastName.value,
            age: updateAge.value ,
            city: updateCity.value
        };
        var params = new URLSearchParams(defaultParams);

        xhr.open('put', 'http://localhost:8080/');
        xhr.send(params);
    }

});

create.addEventListener('click', function() {
    var inputName = document.getElementById('name');
    var inputLastName = document.getElementById('last_name');
    var inputAge = document.getElementById('age');
    var inputCity = document.getElementById('city');
    if(!inputName.value || !inputLastName.value || !inputAge.value || !inputCity.value) {
        alert("Put here your info");
        return;
    } else if(inputAge.value <= 0) {
        alert("Age must be bigger than 0 !!!  Please, be more attentive.");
        return;
    }

    const xhr = new XMLHttpRequest();  //WHY FOR?
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                readFunction();
                inputName.value = '';
                inputLastName.value = '';
                inputAge.value = '';
                inputCity.value ='';
            }
        }
    };

    var defaultParams = {
        name: inputName.value,
        last_name: inputLastName.value,
        age: inputAge.value ,
        city: inputCity.value
    };
    var params = new URLSearchParams(defaultParams);

    xhr.open('post', 'http://localhost:8080/');
    xhr.send(params);

});

readFunction();

function readFunction() {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                const data = JSON.parse(xhr.responseText);
                console.log(data);
                let str = '';
                data.forEach(element => {
                    let tmp = '<tr>';
                    tmp += '<td>' + element.id + '</td>';
                    tmp += '<td><input data-name-id="'+element.id+'" type="text" value="' + element.name + '"></td>';
                    tmp += '<td><input data-last_name-id="'+element.id+'" type="text" value="' + element.last_name + '"></td>';
                    tmp += '<td><input data-age-id="'+element.id+'" type="number" value="' + element.age + '"></td>';
                    tmp += '<td><input data-city-id="'+element.id+'" type="text" value="' + element.city + '"></td>';
                    tmp += '<td><button class="sys-button" id="del" data-id="'+element.id+'">delete</button></td>';
                    tmp += '<td><button class="sys-button" id="upd" data-id="'+element.id+'">Update</button></td>';
                    tmp += '</tr>';
                    str += tmp;
                });
                document.querySelector('tbody').innerHTML = str;
            }
        }
    };
    xhr.open('get', 'http://localhost:8080/', true);
    xhr.send();
}
