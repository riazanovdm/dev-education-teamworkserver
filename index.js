const express = require('express'); //конектим библиотеку експресс
const cors = require('cors');
const pg = require("pg"); // библиотека для работы с постгрес
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const connectionString = "pg://postgres:typeof@localhost:5432/Anton"; // connect to base (password before@ and name after/)
const client = new pg.Client(connectionString); //конект к базе данных
client.connect();



app.get('/', function (req, res) {
    client.query('SELECT * FROM sangvinnik ORDER BY id ASC').then(result => {
        res.send(result.rows);
    }).catch(err => {
        console.log(err.stack);
    });
});

app.post('/', (req, res) => {
    let queryStr = `INSERT INTO sangvinnik (name, last_name, age, city) VALUES ('${req.body.name}', '${req.body.last_name}', '${req.body.age}', '${req.body.city}')`;
    client.query(queryStr).then(result => {
        res.send({'success' : true});
    }).catch(err => {
        res.send({'error' : err.stack});
    });
});

app.put('/', function (req, res) {
    let queryStr = `UPDATE sangvinnik
                    SET
                    name = '${req.body.name}',
                    last_name = '${req.body.last_name}',
                    age = '${req.body.age}',
                    city = '${req.body.city}'
                    WHERE id = '${req.body.id}'`;
    client.query(queryStr).then(result => {
        res.send({'success' : true});
    }).catch(err => {
        res.send({'error' : err.stack});
    });
});

app.delete('/', function (req, res) {
    let queryStr = `DELETE FROM sangvinnik WHERE id='${req.body.id}'`;
    client.query(queryStr).then(result => {
        res.send({'success' : true});
    }).catch(err => {
        res.send({'error' : err.stack});
    });
});

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});